#include "BetterCanvas.h"

#include <iostream>


#include "Circle.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Arrow.h"

/**
 * @brief Casts shape to Circle and draws it
 * @param shape some Shape object
*/
void BetterCanvas::draw_circle(Shape* shape) const
{
	Circle* circle = static_cast<Circle*>(shape);

	this->_canvas.draw_circle(circle->get_center(), circle->getRadius(), circle->get_color());
}

/**
 * @brief Casts shape to Triangle and draws it
 * @param shape some Shape object
*/
void BetterCanvas::draw_triangle(Shape* shape) const
{
	Triangle* triangle = static_cast<Triangle*>(shape);

	this->_canvas.draw_triangle(
		triangle->get_point(0), triangle->get_point(1), triangle->get_point(2),
		triangle->get_color());
}

/**
 * @brief Casts shape to Rectangle and draws it
 * @param shape some Shape object
*/
void BetterCanvas::draw_rectangle(Shape* shape) const
{
	myShapes::Rectangle* rectangle = static_cast<myShapes::Rectangle*>(shape);

	this->_canvas.draw_rectangle(rectangle->get_point(0), rectangle->get_point(1),
		rectangle->get_color());
}

/**
 * @brief Casts shape to Arrow and draws it
 * @param shape some Shape object
*/
void BetterCanvas::draw_arrow(Shape* shape) const
{
	Arrow* arrow = static_cast<Arrow*>(shape);

	this->_canvas.draw_arrow(arrow->get_point(0), arrow->get_point(1), arrow->get_color());
}


/**
 * @brief Casts shape to Circle and removes it from the canvas
 * @param shape some Shape object
*/
void BetterCanvas::remove_circle(Shape* shape) const
{
	Circle* circle = static_cast<Circle*>(shape);

	this->_canvas.clear_circle(circle->get_center(), circle->getRadius());
}

/**
 * @brief Casts shape to Triangle and removes it from the canvas
 * @param shape some Shape object
*/
void BetterCanvas::remove_triangle(Shape* shape) const
{
	Triangle* triangle = static_cast<Triangle*>(shape);

	this->_canvas.clear_triangle(triangle->get_point(0),
		triangle->get_point(1),
		triangle->get_point(2));
}

/**
 * @brief Casts shape to Rectangle and removes it from the canvas
 * @param shape some Shape object
*/
void BetterCanvas::remove_rectangle(Shape* shape) const
{
	myShapes::Rectangle* rectangle = static_cast<myShapes::Rectangle*>(shape);

	this->_canvas.clear_rectangle(rectangle->get_point(0), rectangle->get_point(1));
}

/**
 * @brief Casts shape to Arrow and removes it from the canvas
 * @param shape some Shape object
*/
void BetterCanvas::remove_arrow(Shape* shape) const
{
	Arrow* arrow = static_cast<Arrow*>(shape);

	this->_canvas.clear_arrow(arrow->get_point(0), arrow->get_point(1));
}

void BetterCanvas::draw_shape(Shape* shape)
{
	// get shape type
	std::string t = shape->getType();
	const type shape_type = this->types[t];
	
	switch (shape_type)
	{
	case type::circle:
		draw_circle(shape);
		break;
	case type::triangle:
		draw_triangle(shape);
		break;
	case type::rectangle:
		draw_rectangle(shape);
		break;
	case type::arrow:
		draw_arrow(shape);
		break;
	}
}

void BetterCanvas::crear_shape(Shape* shape)
{
	// get shape type
	std::string t = shape->getType();
	const type shape_type = this->types[t];

	switch (shape_type)
	{
	case type::circle:
		remove_circle(shape);
		break;
	case type::triangle:
		remove_triangle(shape);
		break;
	case type::rectangle:
		remove_rectangle(shape);
		break;
	case type::arrow:
		remove_arrow(shape);
		break;
	}
}
