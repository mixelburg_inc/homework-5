#define _CRT_SECURE_NO_WARNINGS

#include "Menu.h"
#include "Bonus.h"

#include <iostream>
#include <vector>

// BONUS is in the second main in comment below this main
int main()
{
	Menu main_menu;
	main_menu.start();

	return 0;
}

// BONUS
//void main(int argc, char* argv[])
//{
//
//	if (argc < 3)
//	{
//		std::cerr << "Invalid input parameters" << std::endl;
//		std::cerr << "Usage: grep.exe <string to find> <first file path> <second file path> <third file path> ..." << std::endl;
//		std::cerr << "Can be used on 1 or multiple files" << std::endl;
//		exit(1);
//	}
//
//	for (int i = 2; i < argc; ++i)
//	{
//		const std::string file_name = "C:/alice_in_wonderland.txt";
//		const std::string to_find = "tired";
//		grep::find_string(std::string(argv[i]), std::string(argv[1]));
//	}
//}