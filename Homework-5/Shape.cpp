#include "Shape.h"

#include <iostream>

/**
 * @brief builds new Shape object
 * @param name - given name
 * @param type - given type
 * @param color - given color
*/
Shape::Shape(const std::string& name, const std::string& type, const Color color) : _name(name), _type(type), _color(color)
{
}

Shape::~Shape()
{
	this->_name = nullptr;
	this->_type = nullptr;
}

/**
 * @brief prints details about this shape
*/
void Shape::printDetails() const
{
	std::cout << "name: " << this->_name << std::endl << "type: "<< this->_type << std::endl;
}

void Shape::set_color(const Color color)
{
	this->_color = color;
}

Color Shape::get_color() const
{
	return this->_color;
}

// simple getters
std::string Shape::getType() const
{
	return this->_type;
}

std::string Shape::getName() const
{
	return this->_name;
}

