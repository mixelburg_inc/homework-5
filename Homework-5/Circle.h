#pragma once

#include "Shape.h"
#include "Point.h"

#define PI 3.14

class Circle : public Shape
{
public:
	/**
	 * @brief creates new Circle object	 
	 * @param center - point representing center of the circle
	 * @param radius - radius of the circle 
	 * @param type - type of the circle
	 * @param name - name of the circle
	 * @param color - color of the circle
	*/
	Circle(const Point& center, double radius, const std::string& type, const std::string& name, Color color);
	~Circle();

	// Simple getters
	const Point& get_center() const;
	double getRadius() const;

	/**
	 * @brief draws this circle on given Canvas object
	 * @param canvas - given Canvas object 
	*/
	virtual void draw(const Canvas& canvas) override;

	/**
	 * @brief removes this circle from given Canvas object 
	 * @param canvas - given Canvas object
	*/
	virtual void clearDraw(const Canvas& canvas) override;

	/**
	 * @brief calculates area of this circle and returns it
	 * @return calculated area
	*/
	virtual double getArea() const override;

	/**
	 * @brief calculates perimeter of this circle and returns it
	 * @return calculated perimeter
	*/
	virtual double getPerimeter() const override;

	/**
	 * @brief moves this circle based on the given delta Point object 
	 * @param other given delta Point object
	*/
	virtual void move(const Point& other) override;
private:
	Point _center;
	double _radius;
};