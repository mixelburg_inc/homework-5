#pragma once

#include "Shape.h"
#include "Point.h"
#include <vector>

class Polygon : public Shape
{
public:
	/**
	 * @brief creates new Polygon object
	 * @param type - type of the polygon
	 * @param name - name of the polygon
	 * @param color - given color
	*/
	Polygon(const std::string& type, const std::string& name, Color color);

	/**
	 * @brief moves this polygon using given delta point
	 * @param other - given delta point
	*/
	virtual void move(const Point& other) override;

	/**
	 * @brief returns this.points at a given index
	 * @param index - some index
	 * @return point at the specified index
	*/
	Point get_point(int index) const;


protected:
	std::vector<Point> _points;
};