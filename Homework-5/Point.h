#pragma once

class Point
{
public:
	/**
	 * @brief creates new Point object
	 * @param x - x coordinate of the point
	 * @param y - y coordinate of the point
	*/
	Point(double x, double y);
	Point();
	Point(const Point& other);
	virtual ~Point();

	/**
	 * @brief adds this.x to other.x and this.y to other.y
	 * @param other - other Point object
	*/
	void add_x_y(Point& other);
	
	/**
	 * @brief returns new Point object that is sum of this and other points
	 * basically adds this.x to other.x and this.y to other.y
	 * @param other - other given Point object
	 * @return new Point object that is sum of this and other
	*/
	Point operator+(const Point& other) const;

	/**
	 * @brief adds other point to this point and returns pointer to this point
	 * basically adds this.x to other.x and this.y to other.y
	 * @param other - other given Point object
	 * @return pointer to this point
	*/
	Point& operator+=(const Point& other);

	// simple getters and setters
	double getX() const;
	double getY() const;
	void set_x(double x);
	void set_y(double y);
	
	/**
	 * @brief calculates distance from this point to a given point and returns it
	 * @param other given point
	 * @return calculated distance between points
	*/
	double distance(const Point& other) const;
private:
	double _x;
	double _y;
};