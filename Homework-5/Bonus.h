#pragma once

#include <cstdio>
#include <string>

class File
{
public:
	File(const std::string& name, const char* mode);
	~File();

	operator FILE* & ();

private:
	FILE* file_fd;
};

class grep
{
public:
	/**
	 * @brief searches for given string in a given file
	 * @param file_name - given file name
	 * @param to_find - string to find
	*/
	static void find_string(const std::string& file_name, const std::string& to_find);

	/**
	 * @brief checks if given string contains given substring
	 * @param substring - some substring
	 * @param sent - some string
	 * @return - true of false (if string contains substring)_
	*/
	static bool check_for_substring(const char* substring, const char* sent);

private:
	static const int line_size = 1024;
};
